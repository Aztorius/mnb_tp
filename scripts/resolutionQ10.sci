//Affichage des graphes Q10
exec("cholesky.sci")

n = 30
nt = 10
theta = 1/2
l = 10
T = 30
deltak = T/nt
deltax = 2*l/(n+1)
mu = deltak/(deltax)^2
//On crée le vecteur B
vectB = zeros(n,1)
vectB(1) = exp(-(1/2)/l);
//On crée la matrice A
diagA = zeros(n,1);
sDiagA = zeros(n-1,1);
for i=1:n-1
    diagA(i) = exp(-(i+1/2)/l) + exp(-(i-1/2)/l);
    sDiagA(i) = -exp(-(i+1/2)/l);
end
diagA(n) = exp(-(n+1/2)/l) + exp(-(n-1/2)/l);

function [vectU]=solve10(diagA, sDiagA, vectB, k)
// (I + mu * A / 2)*U°(k+1) = (I - mu * A / 2)*U°(k) + mu B
// On calcule M = I + theta * mu * A puis on factorise M par Cholesky
I = eye(n)

//On crée la matrice M
diagM = I + diagA * mu / 2
sDiagM = sDiagA * mu / 2

//On crée la matrice N
diagN = I - diagA * mu / 2
sDiagN = - sDiagA * mu / 2

//On crée le vecteur U(0)
vectU = zeros(n,1)

//On calcule le membre de droite
vectC = zeros(n,1)

for ki=0:k

vectC(1) = diagN(1) * vectU(1) + sDiagN(1) * vectU(2) + mu * vectB(1)
for i=2:n-1
    vectC(i) = sDiagN(i-1) * vectU(i-1) + diagN(i) * vectU(i) + sDiagN(i) * vectU(i+1)
end
vectC(n) = sDiagN(n-1) * vectU(n-1) + diagN(n) * vectU(n)

vectU = cholesky(diagM, sDiagM, vectC)

end

[vectU]=return(vectU)

endfunction

x = [-l+deltax:deltax:l-deltax]'
vectU = solve10(diagA,sDiagA,vectB,50)
clf()
plot(x, vectU)
