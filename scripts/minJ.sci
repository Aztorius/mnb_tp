//Utilise la méthode de Gauss-Newton pour déterminer xd

exec("flux.sci")

epsilon = 10E-5
Fcible = [-0.1, -0.18]

function xd=newton(fctflux, epsilon)
    //Estimation initiale
    xk = [0];
    Fderivatexk = numderivative(fctflux, xk(1))
    Fxk = flux(xk(1))
    
    jderivatexk = (2*(Fderivatexk' * (Fxk - Fcible)))/norm(Fcible, 2)
    k = 0
    while jderivatexk > epsilon
        Fxk = flux(xk(1))
        Fderivatexk = numderivative(fctflux, [xk-1,xk,xk+1])
        deltaxk = (-Fderivatexk' * (Fxk - Fcible))/(Fderivatexk' * Fderivatexk)
        xk(1) = xk(1) + deltaxk
        k = k + 1
        jderivatexk = (2*(Fderivatexk' * (Fxk - Fcible)))/norm(Fcible, 2)
        disp(xk)
    end
    
    xd = return(xk)
endfunction

disp(newton(flux,epsilon))
