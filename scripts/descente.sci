function [vectZ]= descente(diagL, sDiagL, vectY)
    matrixSize = length(diagL);
    // Création du vecteur colonne Z de taille (matrixSizex1)
    vectZ = zeros(matrixSize,1);
    //Cas particulier : Premier element
    vectZ(1) = vectY(1) / diagL(1);
    for i=2:matrixSize
        vectZ(i) = (vectY(i) - sDiagL(i-1) * vectZ(i-1)) / diagL(i);
    end
    [vectZ] = return(vectZ)
endfunction

//descente([1,1,1], [2,2], [1,2,3])
//Résultat attendu : [1,0,3]
