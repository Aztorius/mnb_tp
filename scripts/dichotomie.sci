//Dichotomie pour trouver la valeur de xd
//Ne fonctionne pas à cause d'une erreur dans l'évaluation de fctJ(vectx(j))

exec("resolutionQ12.sci")

epsilon = 10E-5
l = 10
function [xD]= dichotomie(fctJ, epsilon)
    a = -l;
    b = l;
    
    while (b - a) > 2*epsilon
        //Intervalles : xi = a + i * delta
        delta = (b - a) / 4;
        
        for i=1:3
            vectx(i) = a + i * delta;
        end
        
        for i=1:3
            //Erreur dans la valeur récupérée ici
            vectJ(i) = fctJ(a + i * delta)
        end
        
        //Fonction descend puis remonte : le min se trouve entre x(1) et x(3)
        if((vectJ(1) > vectJ(2)) & (vectJ(2) <= vectJ(3)))
            disp("If")
            a = vectx(1);
            b = vectx(3);
        //Fonction strictement décroissante : le min se trouve entre x(2) et b
        elseif((vectJ(1) > vectJ(2)) & (vectJ(2) > vectJ(3)))
            disp("Elseif")
            a = vectx(2);
        //Fonction strictement croissante : le min se trouve entre a et x(2)
        else
            disp("Else")
            b = vectx(2);
        end
        
      disp(vectx)
      disp(a)
      disp(b)
      
    end
    xD = return((a + b) / 2)
    
endfunction

disp(dichotomie(coutJ, epsilon))
