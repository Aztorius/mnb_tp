function [diagL, sDiagL]= factorise(diagM, sDiagM)
    matrixSize = length(diagM);
    // Création des vecteurs colonnes de la matrice triangulaire inférieure L
    diagL = zeros(matrixSize,1);
    sDiagL = zeros(matrixSize-1,1);
    //Cas particulier : Premier element
    diagL(1) = sqrt(diagM(1));
    for i=1:matrixSize-1
        sDiagL(i) = sDiagM(i) / diagL(i);
        diagL(i+1) = sqrt(diagM(i+1) - sDiagL(i)^2);
    end
    [diagL, sDiagL] = return(diagL, sDiagL)
endfunction

//factorise([1,5,5], [2,2])
//Résultat attendu : [1,1,1] sur la diagonale et [2,2] sur la sous diagonale
