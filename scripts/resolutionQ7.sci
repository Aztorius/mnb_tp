//Question 7
//On veut résoudre AU = B
//On va dans un premier temps générer les matrices A et B avec leurs valeurs
//Puis utiliser la méthode de Cholesky pour trouver U
exec("cholesky.sci")

n = 2
l = 5
vectB = zeros(n,1);
vectB(1) = exp(-(1/2)/l);
diagA = zeros(n,1);
sDiagA = zeros(n-1,1);
for i=1:n-1
    diagA(i) = exp(-(i+1/2)/l) + exp(-(i-1/2)/l);
    sDiagA(i) = -exp(-(i+1/2)/l);
end
diagA(n) = exp(-(n+1/2)/l) + exp(-(n-1/2)/l);

//Solution explicite de l'equation (13)
vectV = zeros(n,1);
vectX = zeros(n,1);
deltax = 2*l/(n+1);
for i=1:n
    vectX(i) = i*deltax-l;
    vectV(i) = (exp(vectX(i)/l)-exp(1))/(exp(-1)-exp(1));
end

vectU = cholesky(diagA, sDiagA, vectB);

diff = zeros(n,1);

for i=1:n
    diff(i)=abs(vectU(i)-vectV(i));
end

normeinfinie = max(diff)

//La norme infinie augmente quand n augmente (deltax diminue)

clf()
plot(vectX,vectV)
plot(vectX,vectU)
