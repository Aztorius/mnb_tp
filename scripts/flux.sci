//Calcul du flux Q11

exec("factorise.sci")
exec("descente.sci")
exec("remonte.sci")

a = 0.8;
l = 10;
T = 60;
n = 2000;
deltax = 2*l/(n+1);
nt = 3000;
deltat = T/nt;
mu = deltat/(deltax)^2;
theta = 1/2;

function [U1]=solve11(diagA, sDiagA, k, xd)
// (I + theta * mu * A)*U°(k+1) = (I + (theta - 1) * mu * A)*U°(k) + mu B
// On calcule M = I + theta * mu * A puis on factorise M par Cholesky

//On crée le vecteur B
vectB = zeros(n,1)

//On crée la matrice M
diagM = 1 + theta * diagA * mu
sDiagM = theta * sDiagA * mu

//On crée la matrice N
diagN = 1 + (theta - 1) * diagA * mu
sDiagN = (theta - 1) * sDiagA * mu

[diagL, sDiagL] = factorise(diagM, sDiagM);

//On crée le vecteur U(0)
vectU = zeros(n,1)

//On calcule le membre de droite
vectC = zeros(n,1)

for ki=0:k
vectB(1) = (1 - a*exp(-(-l + deltax/2 - xd)^2 / 4)) * (((ki+1)*deltat/T)^2 * theta + (ki*deltat/T)^2 * (1 - theta));
vectC(1) = diagN(1) * vectU(1) + sDiagN(1) * vectU(2) + mu * vectB(1)
vectC(2:n-1) = sDiagN(1:n-2) .* vectU(1:n-2) + diagN(2:n-1) .* vectU(2:n-1) + sDiagN(2:n-1) .* vectU(3:n)
vectC(n) = sDiagN(n-1) * vectU(n-1) + diagN(n) * vectU(n)

vectZ = descente(diagL, sDiagL, vectC);
vectU = remonte(diagL, sDiagL, vectZ);

end
//Première valeur de u1 à l'instant tinter
U1inter = vectU(1)

for ki=k+1:nt-1
vectB(1) = (1 - a*exp(-(-l + deltax/2 - xd)^2 / 4)) * (((ki+1)*deltat/T)^2 * theta + (ki*deltat/T)^2 * (1 - theta));
vectC(1) = diagN(1) * vectU(1) + sDiagN(1) * vectU(2) + mu * vectB(1)
vectC(2:n-1) = sDiagN(1:n-2) .* vectU(1:n-2) + diagN(2:n-1) .* vectU(2:n-1) + sDiagN(2:n-1) .* vectU(3:n)
vectC(n) = sDiagN(n-1) * vectU(n-1) + diagN(n) * vectU(n)

vectZ = descente(diagL, sDiagL, vectC);
vectU = remonte(diagL, sDiagL, vectZ);

end
//Deuxième valeur de u1 à l'instant tfin
U1fin = vectU(1)

U1=return([U1inter,U1fin])

endfunction

function [Ft]=flux(xd)
    //On crée la matrice A
    diagA = zeros(n,1);
    sDiagA = zeros(n-1,1);
    for i=1:n-1
        diagA(i) = 1 - a*exp(-((i+1/2)*deltax-l-xd(1))^2 / 4) + 1 - a*exp(-((i-1/2)*deltax-l-xd(1))^2 / 4);
        sDiagA(i) = -1 + a*exp(-((i+1/2)*deltax-l-xd(1))^2 / 4);
    end
    diagA(n) = 1 - a*exp(-((n+1/2)*deltax-l-xd(1))^2 / 4) + 1 - a*exp(-((n-1/2)*deltax-l-xd(1))^2 / 4);
    
    tinter = (2/3) * T;
    tfin = T;
    U1 = solve11(diagA, sDiagA, floor(tinter / deltat), xd(1));
    Ftinter = (1-a*exp(-(-l+deltax/2-xd(1))^2 / 4)) * (U1(1)-(2/3)^2)/deltax - deltax * 2/3;
    Ftfin = (1-a*exp(-(-l+deltax/2-xd(1))^2 / 4)) * (U1(2)-1)/deltax - deltax;
    Ft = return([Ftinter,Ftfin])
endfunction
