//Implémentation de la factorisation de Cholesky
exec("factorise.sci")
exec("descente.sci")
exec("remonte.sci")

function [vectU]= cholesky(diagA, sDiagA, vectB)
    matrixSize = length(diagA);
    vectU = zeros(matrixSize,1);
    [diagL, sDiagL] = factorise(diagA, sDiagA);
    vectZ = descente(diagL, sDiagL, vectB);
    vectU = remonte(diagL, sDiagL, vectZ);
    [vectU] = return(vectU)
endfunction
