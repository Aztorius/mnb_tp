//Trace la courbe J(xd)
exec("flux.sci")

function [cout]=coutJ(xd)
    Fcible = [-0.1, -0.18];
    cout = return((norm(flux(xd) - Fcible, 2)/norm(Fcible, 2))^2)
endfunction

//xd = [-6:9/50:3];
//clf()
//xabsciss = [];
//xordinate = [];
//for x = xd
//    xabsciss = cat(1,xabsciss,x);
//    xordinate = cat(1,xordinate,coutJ(x))
//    plot(xabsciss, xordinate);
//end
