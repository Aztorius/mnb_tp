function [vectX]= remonte(diagL, sDiagL, vectZ)
    matrixSize = length(diagL);
    // Création du vecteur colonne Z de taille (matrixSizex1)
    vectX = zeros(matrixSize,1);
    //Cas particulier : Dernier element
    vectX(matrixSize) = vectZ(matrixSize) / diagL(matrixSize);
    for i=matrixSize-1:-1:1
        vectX(i) = (vectZ(i) - sDiagL(i) * vectX(i+1)) / diagL(i);
    end
    [vectX] = return(vectX)
endfunction

//remonte([1,1,1], [2,2], [1,0,3])
//Résultat attendu : [13,-6,3]
